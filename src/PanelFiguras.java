import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;

public class PanelFiguras extends Panel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Figura v[];
	int cont;
	static final int max = 1000; 
	Image aux;
	

	public PanelFiguras(Applet a){
		cont = 0;
		v = new Figura[max];
		aux = a.createImage(600, 400);
	
	}
	
	public boolean agregarFigura(Figura f){
		if (cont == 1000)
			return false;
		else{
			v[cont ++] = f;
			return true;
		}
	}
	
	public boolean eliminarFigura(int i){
		if (cont == 0 || i >= cont)
			return false;
		else{
			for (int j=i; j<cont; j++)
				v[j] = v[j+1];
			cont--;
			return true;
		}
	}
	public void mover(){
		for (int i=0; i<cont; i++)
			v[i].mover();
	}
	
	public void update (Graphics g){
		paint (g);
	}
	public void paint(Graphics g){
		aux.getGraphics().setColor(Color.white);
		aux.getGraphics().fillRect(0, 0, 600, 400);
		for (int i=0; i<cont; i++)
		v[i].paint(aux.getGraphics());
		g.drawImage(aux, 0, 0, this);
	}

}
