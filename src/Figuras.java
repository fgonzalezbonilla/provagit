import java.applet.Applet;
import java.awt.event.*;
import java.awt.*;

public class Figuras extends Applet implements Runnable, ActionListener { 
	
	PanelFiguras  panelIzquierdo;
	Thread t;
	List lstFiguras;
	TextField txtAncho,
			  txtAlto,
			  txtVelocidad,
			  txtMinX,
			  txtMinY,
			  txtMaxX,
			  txtMaxY;
	Choice	  cboMovimiento,
			  cboColor,
			  cboLados;
	Button	  cmdAgregar,	
			  cmdModificar,
			  cmdEliminar;
	CheckboxGroup            cbgTipo;
	Checkbox            chkRectangulo,
						chkElipse,
						chkPoligono;
	Color 				color[];
	
	public void init(){
		
		setLayout(new BorderLayout());
		setSize(600, 400);
		//Crear el panel Derecho
		Panel panelDerecho = new Panel();
		panelDerecho.setBackground(Color.gray);
		panelDerecho.setPreferredSize(new Dimension(200, 400));
		//Añadir la etiqueta "Figuras" al panel derecho
		Label lblAux = new Label("Figuras");
		lblAux.setPreferredSize(new Dimension(200, 20));
		lblAux.setAlignment(Label.CENTER);
		panelDerecho.add(lblAux);
		//Añadir la lista para las figuras al panel derecho
		lstFiguras = new List();
		lstFiguras.setPreferredSize(new Dimension(170, 170));
		panelDerecho.add(lstFiguras);
		//Añadir campos de datos
		txtAncho = agregarTextField(panelDerecho, "Ancho");
		txtAlto = agregarTextField(panelDerecho, "Alto");
		cboMovimiento= agregarChoice(panelDerecho, "Movimiento");
		cboMovimiento.add("Horizontal");
		cboMovimiento.add("Vertical");
		cboMovimiento.add("Diagonal");
		cboMovimiento.add("Diagonal Inversa");
		cboMovimiento.add("En L");
		cboMovimiento.add("En L Inversa");
		txtVelocidad = agregarTextField(panelDerecho, "Velocidad");
		txtMinX = agregarTextField(panelDerecho, "Min X");
		txtMinY = agregarTextField(panelDerecho, "Min Y");
		txtMaxX = agregarTextField(panelDerecho, "Max X");
		txtMaxY = agregarTextField(panelDerecho, "Max Y");
		cboColor= agregarChoice(panelDerecho, "Color");
		cboColor.add("Blanco");
		cboColor.add("Azul");
		cboColor.add("Rojo");
		cboColor.add("Verde");
		cboColor.add("Amarillo");
		cboColor.add("Naranja");
		cboColor.add("Rosa");
		cboColor.add("Gris");
		cboColor.add("Magenta");
		color = new Color[9];
		color[0] = Color.white;
		color[1] = Color.blue;
		color[2] = Color.red;
		color[3] = Color.green;
		color[4] = Color.yellow;
		color[5] = Color.orange;
		color[6] = Color.pink;
		color[7] = Color.gray;
		color[8] = Color.magenta;
		//Añadir tipo de figura
		cbgTipo = new CheckboxGroup();
		chkRectangulo = new Checkbox("Rectangulo",cbgTipo, true);
		chkRectangulo.setPreferredSize(new Dimension(150, 20));
		panelDerecho.add(chkRectangulo);
		chkElipse = new Checkbox("Elipse",cbgTipo, false);
		chkElipse.setPreferredSize(new Dimension(150, 20));
		panelDerecho.add(chkElipse);
		chkPoligono = new Checkbox("Polígono", cbgTipo, false);
		chkPoligono.setPreferredSize(new Dimension(70, 20));
		panelDerecho.add(chkPoligono);
		cboLados = new Choice();
		cboLados.setPreferredSize(new Dimension (75, 20));
		for ( int i=3; i<=9; i++)
			cboLados.add(i + " lados");
		panelDerecho.add(cboLados);
		//Añadir botones
		cmdAgregar = new Button ("Agregar");
		cmdAgregar.addActionListener((this));
		cmdModificar = new Button ("Modificar");
		cmdModificar.addActionListener((this));
		cmdEliminar = new Button ("Eliminar");
		cmdEliminar.addActionListener((this));
		cmdAgregar.setPreferredSize(new Dimension(60,30));
		cmdModificar.setPreferredSize(new Dimension(60,30));
		cmdEliminar.setPreferredSize(new Dimension(60,30));
		panelDerecho.add(cmdAgregar);
		panelDerecho.add(cmdModificar);
		panelDerecho.add(cmdEliminar);
		//Añadir el panel Derecho al Applet
		add(panelDerecho, BorderLayout.EAST);
		//Crear el panel izquierdo
		panelIzquierdo = new PanelFiguras(this);
		add(panelIzquierdo, BorderLayout.CENTER);
		
	}
	
	
	private TextField agregarTextField(Panel p, String e){
		Label lblAux= new Label(e);
		TextField txtAux = new TextField();
		lblAux.setPreferredSize(new Dimension (90, 20));
		txtAux.setPreferredSize(new Dimension (90, 20));
		p.add(lblAux);
		p.add(txtAux);
		return txtAux;
	}
	
	private Choice agregarChoice(Panel p, String e){
		Label lblAux= new Label(e);
		Choice cboAux = new Choice();
		lblAux.setPreferredSize(new Dimension (90, 20));
		cboAux.setPreferredSize(new Dimension (90, 20));
		p.add(lblAux);
		p.add(cboAux);
		return cboAux;
	}

	public void start(){
		t = new Thread(this);
		if (t != null) t.start();
	} 

	public void run(){
		while (true){
			panelIzquierdo.repaint();
			panelIzquierdo.mover();
			try {
				Thread.sleep(40);
			} catch (Exception e) {
			
			}
		}
	} 
	
	public void actionPerformed (ActionEvent e){
		if (e.getSource() == cmdAgregar){
			Figura aux;
			
			if(cbgTipo.getSelectedCheckbox()== chkRectangulo){
				lstFiguras.add("Rectangulo");
				aux = new Rectangulo (Integer.parseInt(txtAncho.getText()),
						Integer.parseInt(txtAlto.getText()),
						Integer.parseInt(txtVelocidad.getText()),
						cboMovimiento.getSelectedIndex(),
						Integer.parseInt(txtMinX.getText()),
						Integer.parseInt(txtMinY.getText()),
						Integer.parseInt(txtMaxX.getText()),
						Integer.parseInt(txtMaxY.getText()),
						color[cboColor.getSelectedIndex()]);
	
			}
		else if (cbgTipo.getSelectedCheckbox()== chkElipse){
			lstFiguras.add("Elipse");
			aux = new Elipse (Integer.parseInt(txtAncho.getText()),
					Integer.parseInt(txtAlto.getText()),
					Integer.parseInt(txtVelocidad.getText()),
					cboMovimiento.getSelectedIndex(),
					Integer.parseInt(txtMinX.getText()),
					Integer.parseInt(txtMinY.getText()),
					Integer.parseInt(txtMaxX.getText()),
					Integer.parseInt(txtMaxY.getText()),
					color[cboColor.getSelectedIndex()]);
		}
		else{
			lstFiguras.add("Poligono");
			aux = new Poligono (cboLados.getSelectedIndex()+3,
					Integer.parseInt(txtAncho.getText()),
					Integer.parseInt(txtVelocidad.getText()),
					cboMovimiento.getSelectedIndex(),
					Integer.parseInt(txtMinX.getText()),
					Integer.parseInt(txtMinY.getText()),
					Integer.parseInt(txtMaxX.getText()),
					Integer.parseInt(txtMaxY.getText()),
					color[cboColor.getSelectedIndex()]);
			}
		//añadir la figura al panel izquierdo
		panelIzquierdo.agregarFigura(aux);
		}
		
		else if (e.getSource () == cmdModificar){
			
		}
		else if (e.getSource ()== cmdEliminar){
			int i =lstFiguras.getSelectedIndex();
			if (i >= 0){
				panelIzquierdo.eliminarFigura(i);
				lstFiguras.remove(i);
			}
			}
		}
	}
