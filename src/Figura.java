import java.awt.Color;
import java.awt.Graphics;

public abstract class Figura {
	
	int x, 		// coordenada X
	    y, 		// coordenada Y
	    v, 		// velocidad de movimiento
	    vx,     // componentes x de la velocidad
	    vy,		// componentes y de la velocidad
	    ancho,	//ancho de la figura
	    alto,	//alto de la figura
	    tipo, 	// tipo de movimiento
	    minX,   // valor minimo para la coordenada X
	    minY,	// valor minimo para la coordenada Y
	    maxX,	// valor maximo para la coordenada X
	    maxY;	// valor maximo para la coordenada Y

	Color color;	// color de relleno de la figura
	
	public Figura (int ancho, int alto, int v, int tipo, 
			int minX, int minY, int maxX, int maxY, Color color){	
		
		this.v = v;
		this.ancho = ancho;
		this.alto= alto;
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
		this.color = color;
		this.tipo= tipo;
		double anchoMov = maxX - minX + 1;
		double altoMov = maxY - minY +1;
		double d = Math.sqrt (Math.pow(anchoMov, 2)+
				Math.pow(altoMov, 2));
		switch (tipo){//tipos de movimiento
			case 0://horizontal restringido a un area rectangular
				x = minX ;
				y = (minY + maxY + 1 -alto);
				vx = v;
				vy = 0;
				break;
			case 1:	//Movimiento vertical restringido a un area rectangular
				x = (minX + maxX - ancho +1) /2 ;
				y = minY;
				vx = 0;
				vy = v;
				break;
			case 2://Movimiento diagonal 
				x = minX ;
				y = minY;
				vx = (int) Math.round(anchoMov * v / d);
				vy = (int) Math.round(altoMov * v / d);
				break;
			case 3://Movimiento diagonal invertido 
				x = maxX - ancho;
				y = minY;
				vx = -(int) Math.round(anchoMov * v / d);
				vy = (int) Math.round(altoMov * v / d);
				break;
			case 4://Movimiento L 
				x = minX ;
				y = minY;
				vx = 0;
				vy = -v;
				break;
			case 5://Movimiento L invertida 
				x = minX ;
				y = minY;
				vx = v;
				vy = 0;
				break;
		
		}
	}
		

	
	public abstract void paint (Graphics g);
	

	public void mover(){
		switch (tipo) {
		case 0: moverH(); break;
		case 1: moverV(); break;
		case 2: moverD(); break;
		case 3: moverDI (); break;
		case 4: moverL (); break;
		case 5: moverLI (); break;
		}
	}
	
	private void moverH(){
		x += vx;
		
		if (x + ancho >= maxX){
			x = maxX - ancho;
			vx = -vx;
		}
		else if(x <= minX){
			x = minX;
			vx = -vx; 
		}
	}
	
	private void moverV(){
		y += vy;
		if (y + alto >= maxY){
			y = maxY - alto;
			vy = -vy;
		}
		else if(y <= minY){
			y = minY;
			vy = -vy;
		}
	}
	
	private void moverD(){
		x += vx ;
		y += vy ;
		
		if ((x+ ancho) >= maxX || (y + alto) >= maxY){
			x = maxX - ancho;
			y = maxY - alto;
			vx = -vx;
			vy = -vy;
		}
		else if (x <= minX || y <= minY){
			x = minX;
			y= minY;
			vx = -vx; 
			vy = -vy;
		}
	
	}
	
	private void moverDI(){
		x += vx ;
		y += vy ;
		
		if (x <= minX ||  (y + alto) >= maxY){
			x = minX;
			y = maxY - alto;
			vx = -vx;
			vy = -vy;
		}
		else if ((x + ancho) >= maxX ||  y <= minY){
			x = maxX - ancho;
			y =  minY;
			vx = -vx; 
			vy = -vy;
		}
	
	}
	
	private void moverL(){
		x += vx;
		y +=vy;
	
		if (y + alto >= maxY && vy > 0){
			y = maxY - alto;
			vx = v;
			vy = 0;
		}
		else if(x + ancho >= maxX && vx > 0){
			x = maxX - ancho;
			vx = -vx;
		}
		else if (x <= minX && vx < 0){
			x= minX;
			vx = 0;
			vy = -v;
		}
		else if(y <= minY && vy < 0 ){
			y = minY;
			vy = -vy;
		}
	}
	
	private void moverLI(){
		x += vx;
		y +=vy;
	
		
		if (x + ancho >= maxX && vx > 0){
			x = maxX - ancho;
			vx = 0;
			vy = -v;
			vy = v;
		
		}
		else if(y + alto >= maxY && vy > 0 ){
			y = maxY - alto;
			vy = -vy;
			
		}
		else if (y <= minY && vy < 0){
			
			y = minY;
			vy = 0;
			vx = -v;	
		}
		else if(x <= minX && vx < 0){
			x= minX;
			vx = -vx;	
		}
	}
	
}
