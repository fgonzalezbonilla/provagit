

import java.awt.Color;
import java.awt.Graphics;

public class Elipse extends Figura {

	public Elipse(int ancho, int alto, int v, int tipo, int minX, int minY,
			int maxX, int maxY, Color color) {
		super(ancho, alto, v, tipo, minX, minY, maxX, maxY, color);

	}


	public void paint(Graphics g) {
		g.setColor(color);
		g.fillOval(x, y, ancho, alto);
	}

}
