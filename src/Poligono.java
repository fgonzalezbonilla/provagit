import java.awt.Color;
import java.awt.Graphics;

public class Poligono extends Figura
{

	int px[], py[];
	
	public Poligono (int lados, int radio, int v, int tipo, int minX, int minY,
			int maxX, int maxY, Color color) {
		super(radio, radio, v, tipo, minX, minY, maxX, maxY, color);
		px = new int [lados];
		py = new int [lados];
		double inc = 2*Math.PI/ (double)lados;
        double angulo = 0.0;
        
    	for (int i=0; i<lados; i++){    
			px[i] =(int)((int)Math.round(radio * Math.cos(angulo)) + radio + x);
			py[i] =(int)((int)Math.round(radio * Math.sin(angulo)) + radio + y);
			angulo += inc;
		}
	}
 
    public void paint(Graphics g)
    {
        g.setColor(color);
        g.fillPolygon(px, py, px.length);
    }
    
   public void mover(){
		int xaux = x, yaux= y;
		super.mover();
		int dx = x -xaux , dy = y - yaux;
		for (int i = 0; i<px.length; i++){
			px[i] += dx;
			py[i] += dy;
		}
    }
}